
var Grupo = function(_clave, _nombre, _docente, _materias, _alumnos){
	return {
		"clave":_clave,
		"nombre":_nombre,
		"docente":_docente,
		"materias":_materias,
		"alumnos":_alumnos
	};
};




/*----------FUNCIONES DE GRUPOS---------*/

var Grupos = [];

function agregarNuevoGrupo(g){
    if(!existeUnGrupo(grupo1.clave)){
        Grupos.push(g);
        return{
            "Exito":true,
            "Mensaje":"Grupo Agregado Exitsamente"
        }
    }
    return {
        "Exito":false,
        "Mensaje":"El Grupo no se pudo agregar"    
    }
}


function existeUnGrupo(cg){
    for(var i; i<Grupos.length; i++){
        if(Grupos[i].clave === cg){
            return true
        }
    }
    return false;
}

agregarNuevoGrupo(grupo1);
existeUnGrupo('M23');
var grupo1 = Grupo('M23','ISA');
var grupo2 = Grupo('M56','ISB');


/*++++++++++++++++++++++++++++++++++++++++*
 *++++++++++++++++++++++++++++++++++++++++*
 *----------FUNCIONES DE DOCENTES---------*
 *++++++++++++++++++++++++++++++++++++++++*
 *++++++++++++++++++++++++++++++++++++++++*/

var Docente = function(_clave, _nombre, _apellidos, _grado_academico){
	return{
		"clave":_clave,
		"nombre":_nombre,
		"apellidos":_apellidos,
		"grado_academico":_grado_academico
	};
};

//C -> Agregar Docente
function asignarDocente(g, d){
    if(!g.docente){
        g.docente = d;
        return {
            "exito": true,
            "mensaje": "Docente agregado al grupo: " + g.nombre
        };
    }
    return {
        "exito": false,
        "mensaje": "El grupo " + g.nombre + " ya tiene docente asignado."
    };
}


//R -> Leer o imprimir datos de un docente

function leerDocente(g, cd){
    if(g.docente){
        return{
            "Clave":g.docente.clave,
            "Nombre":g.docente.nombre,
            "Apellidos":g.docente.apellidos,
            "Grado Academico":g.docente.grado_academico
        }
    }
    return{
        "Exito":false,
        "Mensaje":"El maestro no esta asignado a ese grupo"
    }
}   


//U -> Actualizar los datos del profesor

function actualizarDocente(g, cd, _clave, _nombre, _apellidos , _grado_academico){
    if(g.docente || g.docente.clave===cd){
        g.docente.clave=_clave;
        g.docente.nombre=_nombre;
        g.docente.apellidos=_apellidos;
        g.docente.grado_academico=_grado_academico;
    }
    return{
        "Exito":false,
        "Mensaje":"El Docente que desea actualizar no esta asignado a este grupo o no existe"
    }
}

function eliminarDocente(g,cd){
    
}


//se busca un docente dentro del objeto del grupo que se le asigne como parametro
function docenteAsignado(g,_cl){
    if(g.docente.clave === _cl){
        return {
          "exito": true,
          "mensaje": "El docente si se encuentra asignado a este grupo"
        };
    }
    return {
        "exito": false,
        "mensaje": "El docente no se encuentra asignado a este grupo"
    }
}

var docente1 = Docente('M20','Jose', 'Guzman Gordillo','Ingeniero');

asignarDocente(grupo1,docente1);
leerDocente(grupo1,docente1);
actualizarDocente(grupo1, 'M22','M20','juan','perez','Licenciado');


/*++++++++++++++++++++++++++++++++++++++++*
 *++++++++++++++++++++++++++++++++++++++++*
 *----------FUNCIONES DE MATERIAS---------*
 *++++++++++++++++++++++++++++++++++++++++*
 *++++++++++++++++++++++++++++++++++++++++*/
 
//C -> Crea Objetos de Materias 
var Materia = function(_clave,	_nombre){
	return{
		"clave":_clave,
		"nombre":_nombre
	};
};

function asignarMateriaAlGrupo(g, m){
    if(!g.materias){
        g.materias = [];
    }
    if (!existeMateriaEnGrupo(g, m.clave)){
        g.materias.push(m);
        return {
            "Exito":true,
            "Mensaje":"Materia asignada correctamente al Grupo:"+g.nombre
        }
    }
    return {
        "Exito":false,
        "Mensaje":"La Materia no se pudo agregar Correctamente"
    }
}

//R -> Busca si la materia ya existe en el grupo
function existeMateriaEnGrupo(g, cm){
    if(!g.materias || g.materias.length === 0){
        return {
            "Exito":false,
            "Mensaje":"La Materia no Existe"};
    }
    for (var i = 0; i<g.materias.length; i++){
        if(g.materias[i].clave === cm){
            return i;
        }
        return false;
    }
}

//U -> Actualiza los datos de la Materia de acuerdo a su clave

function actualizarMateria(g,cm, _clave, _nombre){
    if (existeMateriaEnGrupo(g,cm)){
        docentes[aux].clave = _clave;
        docentes[aux].nombre = _nombre;
        return {
            "Exito":true,
            "Mensaje":"Datos actualizados correctamente"
        }
    }
    return {
        "Exito":false,
        "Mensaje":"No se Pudieron actualizar los datos"
    }
}


function eliminarMateriaEnGrupo(g,m){
    if(g.materias){
        if(existeMateriaEnGrupo(g,m)){
            g.materia.shift();
            return{
                "Exito":true,
                "Mensaje":"Materia eliminada satisfactoriamente"
            }
            return{
                "Exito":false,
                "Mensaje":"La materia no se pudo eliminar"
            }
        }
    }
}

var materia1 = Materia('AEF10','Simulacion');
var materia2 = Materia('AEF12','Graficacion');
var materia3 = Materia('AEF23','POO');

asignarMateriaAlGrupo(grupo2,materia3);
actualizarMateria(grupo2,'AEF10','AEF11','Estructura de Datos');
existeMateriaEnGrupo(grupo2,'AEF10');

//funcion que crea Objetos de las Calificaciones
var Calificaciones = function (_unidad1, _unidad2, _unidad3, _unidad4){
	return{
		"unidad1":_unidad1,
		"unidad2":_unidad2,
		"unidad3":_unidad3,
		"unidad4":_unidad4
	};
};


//funcion que crea Objetos de Alumnos
var Alumnos = function (_clave, _nombre, _apellidos, _calificaciones){
	return{
		"clave":_clave,
		"nombre":_nombre,
		"apellidos":_apellidos,
		"calificaciones":_calificaciones
	};
}

